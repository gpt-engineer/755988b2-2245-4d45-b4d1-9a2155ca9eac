document.addEventListener("DOMContentLoaded", function () {
  const flashcard = document.getElementById("flashcard");
  const question = document.getElementById("question");
  const answer = document.getElementById("answer");
  const generate = document.getElementById("generate");

  flashcard.addEventListener("click", function () {
    answer.classList.toggle("hidden");
  });

  generate.addEventListener("click", function () {
    fetch("https://filipstal.pythonanywhere.com/randomqa")
      .then((response) => response.json())
      .then((data) => {
        question.textContent = data.question;
        answer.textContent = data.answer;
        answer.classList.add("hidden");
      });
  });

  const score1 = document.getElementById("score1");
  const score2 = document.getElementById("score2");
  const increase1 = document.getElementById("increase1");
  const increase2 = document.getElementById("increase2");
  const reset = document.getElementById("reset");

  increase1.addEventListener("click", function () {
    score1.textContent = parseInt(score1.textContent) + 1;
  });

  increase2.addEventListener("click", function () {
    score2.textContent = parseInt(score2.textContent) + 1;
  });

  reset.addEventListener("click", function () {
    score1.textContent = 0;
    score2.textContent = 0;
  });
});
